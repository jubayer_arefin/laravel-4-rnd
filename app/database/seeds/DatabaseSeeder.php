<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('TransactionsTableSeeder');
		$this->call('DogsTableSeeder');
		$this->call('TweetsTableSeeder');
		$this->call('PostsTableSeeder');
		$this->call('UsersTableSeeder');
	}

}