<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', function() {
            return View::make('hello');
        });
//Route::get('user/{id}', function($id) {
//            return View::make('users');
//        });
//Route::get('all', function() {
//            $url = URL::to('users');
//            return $url;
//        });
//Route::get('notall/{name}/{location?}', 'HomeController@index')->where('name', '[A-Z a-z]+')->where('location', '[A-Z a-z]+');
//Route::get('user/{id}', 'UserController@showProfile');

Route::resource('tweets', 'TweetsController');

Route::resource('users', 'UsersController');