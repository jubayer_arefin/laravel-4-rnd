<?php

return array(
	/**
	 * Model title
	 *
	 * @type string
	 */
	'title' => 'Users',
	/**
	 * The singular name of your model
	 *
	 * @type string
	 */
	'single' => 'user',
	/**
	 * The class name of the Eloquent model that this config represents
	 *
	 * @type string
	 */
	'model' => 'User',
	/**
	 * The columns array
	 *
	 * @type array
	 */
	'columns' => array(
		'name' => array(
			'title' => 'Name'
		),
		'address' => array(
			'title' => 'Address'
		)
	),
	/**
	 * The edit fields array
	 *
	 * @type array
	 */
	'edit_fields' => array(
	    'name' => array(
	        'title' => 'Name',
	        'type' => 'text'
	    ),
	    'address' => array(
	        'title' => 'Address',
	        'type' => 'text'
	    )
	),
	/**
	 * The filter fields
	 *
	 * @type array
	 */
	'filters' => array(
	    // 'id',
	    'name' => array(
	        'title' => 'Name',
	    ),
	    'address' => array(
	        'title' => 'Address',
	    ),
	    // 'date' => array(
	    //     'title' => 'Date',
	    //     'type' => 'date',
	    // ),
	)
);