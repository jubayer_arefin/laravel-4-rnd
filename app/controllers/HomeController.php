<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function index($name = null, $location = null) {
        //return View::make('users', array('name' => $name));
        return Response::json(array('name' => $name, 'state' => $location));
    }

    public function showWelcome() {
        return View::make('hello');
    }

    /**
     * Show the profile for the given user.
     */
    public function showProfile($id) {
        $user = User::find($id);

        return View::make('user.profile', array('user' => $user));
    }

    public function missingMethod($parameters) {
        return 'Notfound, probably ..zzZ';
    }

}